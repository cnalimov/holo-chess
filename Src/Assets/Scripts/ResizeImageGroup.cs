﻿using Assets.Scripts.Shared;
using UnityEngine;

namespace Assets.Scripts
{
    public interface IResizeImageGroup : IVisible
    {
    }

    public class ResizeImageGroup : MonoBehaviour, IResizeImageGroup
    {
        void Start()
        {
            Hide();
        }


        public void Show()
        {
            SetVisibility(true);
        }

        public void Hide()
        {
            SetVisibility(false);
        }

        private void SetVisibility(bool isVisible)
        {
            gameObject.SetActive(isVisible);
        }
    }
}