﻿namespace Assets.Scripts.Log
{
    public class Console
    {
        public static string pDocument { get; set; }
        public static void log(string message)
        {
            pDocument += "\n" + message;
        }
    }
}