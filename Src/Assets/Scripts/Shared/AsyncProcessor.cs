﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Shared
{
    public interface IAsyncProcessor
    {
        IAsyncTask Run(IEnumerator routine);
    }

    public class AsyncProcessor : MonoBehaviour, IAsyncProcessor
    {
        
        public IAsyncTask Run(IEnumerator routine)
        {
            var task = new AsyncTask();
            StartCoroutine(InternalRoutine(routine, task));
            return task;
        }

        private IEnumerator InternalRoutine(IEnumerator routine, AsyncTask task)
        {

            yield return StartCoroutine(routine);
            task.Complete();
            
        }

        private class AsyncTask : IAsyncTask
        {
            private List<Action> _actions;

            public AsyncTask()
            {
                _actions = new List<Action>();
            }
            public bool IsCompleted { get; set; }

            public IAsyncTask OnCompleted(Action action)
            {
                if (IsCompleted)
                    action();
                else
                {
                    RegisterCallback(action);
                }
                return this;
            }

            public void Complete()
            {
                IsCompleted = true;
                foreach (var action in _actions)
                {
                    action();
                }
                _actions = new List<Action>();
            }

            private void RegisterCallback(Action action)
            {
                _actions.Add(action);;
            }


        }
    }
}