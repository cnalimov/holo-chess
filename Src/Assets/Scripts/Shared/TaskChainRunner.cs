﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Assets.Scripts.Shared
{
   

    public class TaskChainRunner
    {
        private readonly IAsyncProcessor _asyncProcessor;
//        private readonly Queue<IAsyncTask> _tasks = new Queue<IAsyncTask>();
        private readonly Queue<Func<IAsyncTask>> _tasks = new Queue<Func<IAsyncTask>>();
        private bool _isRunning;
        public TaskChainRunner(IAsyncProcessor asyncProcessor)
        {
            _asyncProcessor = asyncProcessor;
        }


        public void Add(Func<IEnumerator> func)
        {
            var mappedTask = new Func<IAsyncTask>(() => _asyncProcessor.Run(func()));
            Add(mappedTask);
            //Add(_asyncProcessor.Run(func()));
        }

        public void Add(Func<IAsyncTask> mappedTask)
        {
            _tasks.Enqueue(mappedTask);
            StartTaskExecution();
        }

        public void Add(IAsyncTask task)
        {
            Add(()=>task);
            
        }

        private void StartTaskExecution()
        {
            if (_isRunning)
                return;
            _isRunning = true;
            RunThroughTasks();
        }

        private void RunThroughTasks()
        {
            if (_tasks.Count == 0)
            {
                _isRunning = false;
                return;
            }
            var task = _tasks.Dequeue();
            task().OnCompleted(RunThroughTasks);
        }
    }
}