﻿namespace Assets.Scripts.Shared
{
    public interface IVisible
    {
        void Show();
        void Hide();
    }
}