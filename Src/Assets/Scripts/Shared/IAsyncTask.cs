﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Shared
{
    public interface IAsyncTask
    {
        bool IsCompleted { get; set; }
        IAsyncTask OnCompleted(Action action);
    }
}