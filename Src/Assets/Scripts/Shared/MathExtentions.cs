public static class MathExtentions
{
    public static int Bound(this int value, int min, int max)
    {
        value = value < min ? min : value;
        value = value > max ? max : value;
        return value;
    }
}