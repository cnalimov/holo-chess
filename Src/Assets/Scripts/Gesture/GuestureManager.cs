﻿using Assets.Scripts.Signal;
using UnityEngine.VR.WSA.Input;
using Zenject;

namespace Assets.Scripts.Gesture
{
    public class GuestureManager : IInitializable
    {
        private readonly GestureSignal.Trigger _trigger;
        private GestureRecognizer _recognizer;

        public GuestureManager(GestureSignal.Trigger trigger)
        {
            _trigger = trigger;
        }

        public void Initialize()
        {
            _recognizer = new GestureRecognizer();
            _recognizer.SetRecognizableGestures(
                GestureSettings.ManipulationTranslate |
                GestureSettings.Tap);
                
            _recognizer.HoldStartedEvent += (source, ray) =>
            {
                _trigger.Fire(new Signal.Gesture {EventType = Signal.Gesture.Type.HoldStarted});
            };

            _recognizer.HoldCanceledEvent += (source, ray) =>
            {
                _trigger.Fire(new Signal.Gesture { EventType = Signal.Gesture.Type.HoldCompleted });
            };

            _recognizer.HoldCompletedEvent += (source, ray) =>
            {
                _trigger.Fire(new Signal.Gesture { EventType = Signal.Gesture.Type.HoldCompleted });
            };

            _recognizer.ManipulationStartedEvent += (source, position, ray) =>
            {
                _trigger.Fire(new ManipulationGesture {EventType = Signal.Gesture.Type.ManipulationStarted,Position = position});
            };

            _recognizer.ManipulationCompletedEvent += (source, delta, ray) =>
            {
                _trigger.Fire(new ManipulationGesture { EventType = Signal.Gesture.Type.ManipulationCompleted, Position = delta });
            };

            _recognizer.ManipulationCanceledEvent += (source, delta, ray) =>
            {
                _trigger.Fire(new ManipulationGesture { EventType = Signal.Gesture.Type.ManipulationCompleted, Position = delta });
            };
            _recognizer.ManipulationUpdatedEvent += (source, delta, ray) =>
            {
                _trigger.Fire(new ManipulationGesture { EventType = Signal.Gesture.Type.ManipulationUpdated, Position = delta });
            };


            _recognizer.TappedEvent += (source, tapCount, ray) =>
            {
                _trigger.Fire(new Signal.Gesture {EventType = Signal.Gesture.Type.Tapped});
            };
            
            _recognizer.StartCapturingGestures();
        }
    }
}