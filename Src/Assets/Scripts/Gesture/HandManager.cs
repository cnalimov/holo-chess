﻿using System;
using Assets.Scripts.Signal;
using UnityEngine.VR.WSA.Input;
using Zenject;

namespace Assets.Scripts.Gesture
{
    public class HandManager : IInitializable, IDisposable
    {
        private readonly GestureSignal.Trigger _trigger;

        public HandManager(GestureSignal.Trigger trigger)
        {
            _trigger = trigger;
        }
        public void Initialize()
        {
            InteractionManager.SourceDetected += InteractionManager_SourceDetected;
            InteractionManager.SourceLost += InteractionManager_SourceLost;

        }

        private void InteractionManager_SourceLost(InteractionSourceState state)
        {
            _trigger.Fire(new Signal.Gesture { EventType = Signal.Gesture.Type.HandLost });
        }

        private void InteractionManager_SourceDetected(InteractionSourceState state)
        {
            _trigger.Fire(new Signal.Gesture { EventType = Signal.Gesture.Type.HandDetected });
        }

        public void Dispose()
        {
            InteractionManager.SourceDetected -= InteractionManager_SourceDetected;
            InteractionManager.SourceLost -= InteractionManager_SourceLost;
        }
    }
}