﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Board;
using Assets.Scripts.Shared;
using Assets.Scripts.Signal;
using UniRx;

namespace Assets.Scripts
{
    public class ChessGame
    {
        private readonly ChessBoard _board;
        private readonly IAsyncProcessor _asyncProcessor;
        private readonly ICellSelector _cellSelector;
        private readonly GestureSignal _gestureSignal;
        private readonly cgEngine _cgEngine;
        private bool _gameIsPlaying;

        private cgBoard _abstractBoard = new cgBoard();
        private Side _currentTurn = Side.White;
        private List<cgSimpleMove> _legalMoves = new List<cgSimpleMove>();
        private FigureManipulation _figureManipulation;


        public ChessGame(ChessBoard board, 
                        IAsyncProcessor asyncProcessor, 
                        ICellSelector cellSelector,
                        GestureSignal gestureSignal,
                        cgEngine cgEngine)
        {
            _board = board;
            _asyncProcessor = asyncProcessor;
            _cellSelector = cellSelector;
            _gestureSignal = gestureSignal;
            _gestureSignal.Event += OnGestureSignal;
            _cellSelector.GazeCellObservable.Subscribe(HighlightCell);
            _cellSelector.VoiceCellObservable.Subscribe(OnCellHighlightedByVoice);
            _cgEngine = cgEngine;
            _figureManipulation = new FigureManipulation(_board);

            PlacePieces();
        }

        private void OnCellHighlightedByVoice(Coordinate coordinate)
        {
            HighlightCell(coordinate);
            TapOnCell(coordinate);
        }

        private void HighlightCell(Coordinate x)
        {

            if (!_gameIsPlaying) return;

            var figures = _currentTurn == Side.White ? _board.White : _board.Black;
            var figure = figures.Find(x);
            _board.HighlightFigure(figure);
            if (_legalMoves.Count>0 && !MoveIsLegal(x))
                _cellSelector.SetColor(CellSelector.Color.Disabled);
            else
                _cellSelector.SetColor(CellSelector.Color.Active);

        }

        private void MakeMove(Coordinate cell)
        {
            if (_board.SelectedFigure != null && MoveIsLegal(cell))
            {
                _board.MoveSelectedTo(cell);
                MovePeaceInAbstractBoard(cell);
                SwitchTurn();
            }
        }

        private void TapOnCell(Coordinate cell)
        {

            //Go To Deselect state
            if (_board.SelectedFigure != null && _board.HighlightedFigure != null)
            {
                //different figure
                if (_board.SelectedFigure.Coordinate != _board.HighlightedFigure.Coordinate)
                    SelectFigure(_board.HighlightedFigure.Coordinate);
                else
                    SelectFigure(null);

                return;
            }

            //Go To select state
            if (_board.SelectedFigure == null && _board.HighlightedFigure != null)
            {
                SelectFigure(_board.HighlightedFigure.Coordinate);
                return;
            }

            //Make a move
            MakeMove(cell);
        }

        private void OnGestureSignal(Signal.Gesture gesture)
        {
            if (IsFigureSelected() && gesture.IsManipulationGesture())
            {
                var figureManipulation = _figureManipulation.ProcessFigureManipulation(gesture);
                if (figureManipulation == Signal.Gesture.Type.ManipulationCompleted)
                {
                    TapOnCell(_board.SelectedFigure.ManipulationCoordinate);
                }
                return;
            }

            if (gesture.EventType != Signal.Gesture.Type.Tapped) return;
            var cell = _cellSelector.GazeCell;
            TapOnCell(cell);
        }
        

        private bool IsFigureSelected()
        {
            return _board.SelectedFigure != null;
        }

        private void SwitchTurn()
        {
            _currentTurn = _currentTurn == Side.White ? Side.Black : Side.White;
            if (_currentTurn == Side.Black)
            {
                EngineMove();
            }
        }

        private void EngineMove()
        {
            
            _cgEngine.MakeMove(_abstractBoard, 
                              false,
                              move =>
                              {
                                  var from = cgGlobal.PosToString(move.@from);
                                  var to = cgGlobal.PosToString(move.@to);

                                  SelectFigure(Coordinate.Create(@from));
                                  MakeMove(Coordinate.Create(to));
                              });
        }

        private void MovePeaceInAbstractBoard(Coordinate coordinate)
        {
            var to = cgGlobal.IndexFromCellName(coordinate.ToStringPosition());
            var move = _legalMoves.FirstOrDefault(m => m.to == to);
            _abstractBoard.move(move);
        }

        private void SelectFigure(Coordinate coordinate)
        {
            var coordinates = coordinate!=null ? coordinate.ToStringPosition() : null;
            var figure = _board.SelectFigure(coordinates, _currentTurn);
            if (figure != null)
            {
                var fromPosition = cgGlobal.IndexFromCellName(coordinates);
                _legalMoves = _abstractBoard.findStrictLegalMoves(_currentTurn == Side.White)
                                            .Where(m => m.@from == fromPosition)
                                            .ToList();
            }
            else
            {
                _legalMoves = new List<cgSimpleMove>();
            }

            _figureManipulation.SetLegalMoves(_legalMoves);
        }

        private bool MoveIsLegal(Coordinate coordinate)
        {
            return coordinate.MoveIsLegal(_legalMoves);
        }

        public void Stop()
        {
            _cellSelector.Hide();
            _gameIsPlaying = false;
        }

        public void Start()
        {
            _cellSelector.Show();
            _gameIsPlaying = true;
        }

        public void Hide()
        {
            _board.gameObject.SetActive(false);
        }
        public void Show()
        {
            _board.gameObject.SetActive(true);
        }

        public void StartNewGame()
        {
            PlacePieces();
            Start();
        }

        private void PlacePieces()
        {
            _abstractBoard = new cgBoard();
        }
    }
}