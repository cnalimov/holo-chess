﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Signal;
using UnityEngine.Windows.Speech;
using Zenject;

namespace Assets.Scripts.Voice
{
    public class VoiceControl : IInitializable
    {
        private readonly GameSignal.Trigger _gameSignalTrigger;
        private readonly CellSelectionSignal.Trigger _cellSignalTrigger;
        KeywordRecognizer _keywordRecognizer = null;
        readonly Dictionary<string, System.Action> _keywords = new Dictionary<string, System.Action>();

        public VoiceControl(GameSignal.Trigger gameSignalTrigger, CellSelectionSignal.Trigger cellSignalTrigger)
        {
            _gameSignalTrigger = gameSignalTrigger;
            _cellSignalTrigger = cellSignalTrigger;
            RegisterGameSignals();
            RegisterMoveCommands();
        }

        private void RegisterMoveCommands()
        {
            
            var lines = Enumerable.Range(0, 8).Select(index => Coordinate.IndexToLetter(index).ToString()).ToList();
            Enumerable.Range(1,8)
                      .SelectMany(vert => lines.Select(line => line+vert.ToString()))
                      .ToList()
                      .ForEach(cell =>
                {
                    _keywords.Add(cell,()=> { _cellSignalTrigger.Fire(Coordinate.Create(cell)); });
                });
        }

        private void RegisterGameSignals()
        {
            _keywords.Add("Place Board", () => { _gameSignalTrigger.Fire(GameMode.PlaceBoard); });
            _keywords.Add("New Game", () => { _gameSignalTrigger.Fire(GameMode.NewGame); });
        }

        public void Initialize()
        {
            _keywordRecognizer = new KeywordRecognizer(_keywords.Keys.ToArray());
            _keywordRecognizer.OnPhraseRecognized += OnPhraseRecognized;
            _keywordRecognizer.Start();
        }

        private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
        {
            System.Action keywordAction;
            if (_keywords.TryGetValue(args.text, out keywordAction))
            {
                keywordAction.Invoke();
            }
        }
    }
}