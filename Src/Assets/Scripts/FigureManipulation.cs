﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Signal;
using UnityEngine;

namespace Assets.Scripts
{
    public class FigureManipulation
    {
        private readonly ChessBoard _board;
        private Figure _figure;
        private Vector3 _manipulationPreviousPosition;
        private List<cgSimpleMove> _legalMoves;

        public FigureManipulation(ChessBoard board)
        {
            _board = board;
        }

        public void SetLegalMoves(List<cgSimpleMove> legalMoves)
        {
            _legalMoves = legalMoves;
        }

        public Signal.Gesture.Type ProcessFigureManipulation(Signal.Gesture gesture)
        {
            var manipulationGesture = gesture as ManipulationGesture;
            if (manipulationGesture == null)
                return Signal.Gesture.Type.None;

            SetFigure(_board.SelectedFigure);
            Manipulate(manipulationGesture);
            return manipulationGesture.EventType;
        }

        private void Manipulate(ManipulationGesture gesture)
        {
            switch (gesture.EventType)
            {

                case Signal.Gesture.Type.ManipulationStarted:
                    _manipulationPreviousPosition = gesture.Position;
                    break;

                case Signal.Gesture.Type.ManipulationUpdated:
                    Vector3 moveVector = Vector3.zero;
                    moveVector = gesture.Position - _manipulationPreviousPosition;
                    _manipulationPreviousPosition = gesture.Position;
                    //_figure.CalculateCellForManipulation(moveVector);
                    //_figure.Manipulate(moveVector);
                    _figure.ManipulateWithGrid(moveVector,_legalMoves);
                    break;

                case Signal.Gesture.Type.ManipulationCompleted:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetFigure(Figure figure)
        {
            _figure = figure;
        }

    }
}
 