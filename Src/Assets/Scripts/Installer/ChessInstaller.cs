using System;
using Assets.Scripts;
using Assets.Scripts.Board;
using Assets.Scripts.BoardPlacing;
using Assets.Scripts.Gesture;
using Assets.Scripts.Shared;
using Assets.Scripts.Signal;
using Assets.Scripts.Voice;
using UnityEngine;
using Zenject;

public class ChessInstaller : MonoInstaller<ChessInstaller>
{
    public Settings AppSettings;

    public override void InstallBindings()
    {
        Container.BindInstance(AppSettings.Figures);
        Container.BindAllInterfaces<GameManager>().To<GameManager>().AsSingle();
        Container.BindAllInterfaces<VoiceControl>().To<VoiceControl>().AsSingle();

        Container.BindAllInterfacesAndSelf<ChessGame>().To<ChessGame>().AsSingle();
        Container.BindAllInterfacesAndSelf<CellSelector>().To<CellSelector>().AsSingle();
        Container.Bind<cgEngine>().FromGameObject().WithGameObjectName("cgEngineGameObject");
        Container.Bind<TaskChainRunner>();
        

        Container.Bind<IAsyncProcessor>().To<AsyncProcessor>().FromGameObject().AsSingle();
        Container.Bind<GridPositionStrategy>().ToSelf().AsTransient();

        Container.Bind<PlayerFigures.Factory>();
        Container.Bind<Figure.Factory>();

        Container.BindAllInterfacesAndSelf<GuestureManager>().To<GuestureManager>().AsSingle();
        Container.BindAllInterfacesAndSelf<HandManager>().To<HandManager>().AsSingle();
        Container.BindAllInterfacesAndSelf<BoardPlacingControl>().To<BoardPlacingControl>().AsSingle();



        Container.BindSignal<GameSignal>();
        Container.BindTrigger<GameSignal.Trigger>();

        Container.BindSignal<GestureSignal>();
        Container.BindTrigger<GestureSignal.Trigger>();

        Container.BindSignal<CellSelectionSignal>();
        Container.BindTrigger<CellSelectionSignal.Trigger>();
        //        Container.BindAllInterfaces<VoiceCommandHandler>().To<VoiceCommandHandler>().AsSingle();
        //        Container.BindAllInterfaces<PlayerShootHandler>().To<PlayerShootHandler>().AsSingle();
    }

    [Serializable]
    public class Settings
    {
        public FigurePrefabs Figures;
    }

    [Serializable]
    public class FigurePrefabs
    {
        public GameObject Pawn;
        public GameObject Knight;
        public GameObject Bishop;
        public GameObject Rock;
        public GameObject Queen;
        public GameObject King;
    }
}