﻿using Zenject;

namespace Assets.Scripts.Signal
{
    public class GameSignal : Signal<GameMode>
    {
        public class Trigger : TriggerBase { }
    }

    public enum GameMode
    {
        Initial,
        PlaceBoard,
        NewGame
    }

    
}