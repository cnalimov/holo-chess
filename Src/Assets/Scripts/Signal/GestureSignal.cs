﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.Signal
{
    public class GestureSignal : Signal<Gesture>
    {
        public class Trigger : TriggerBase
        {
            
        }
    }

    public class Gesture
    {
        public Type EventType { get; set; }

        public enum Type
        {
            None, 
            Tapped,
            HandDetected,
            HandLost,
            HoldStarted,
            HoldCompleted,
            ManipulationStarted,
            ManipulationCompleted,
            ManipulationUpdated
        }
    }

    public static class GestureExtentions
    {
        public static bool IsManipulationGesture(this Gesture.Type type)
        {
            return type == Gesture.Type.ManipulationStarted ||
                   type == Gesture.Type.ManipulationCompleted ||
                   type == Gesture.Type.ManipulationUpdated;
        }
        public static bool IsManipulationGesture(this Gesture gesture)
        {
            return IsManipulationGesture(gesture.EventType);
        }
    }

    public class ManipulationGesture : Gesture
    {
        public Vector3 Position { get; set; }
    }
}