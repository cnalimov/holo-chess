﻿using System;
using Zenject;

namespace Assets.Scripts.Signal
{
    public class CellSelectionSignal : Signal<Coordinate>
    {
        public class Trigger : TriggerBase
        {

        }
    }
}