﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.Gaze
{
    public class GazeControl : ITickable
    {
        private readonly Camera _camera;
        private GameObject FocusedObject { get; set; }


        public GazeControl(Camera camera)
        {
            _camera = camera;
        }

        public void Tick()
        {
            var oldFocusObject = FocusedObject;

            // Do a raycast into the world based on the user's
            // head position and orientation.
            var headPosition = _camera.transform.position;
            var gazeDirection = _camera.transform.forward;

            RaycastHit hitInfo;
            FocusedObject = Physics.Raycast(headPosition, gazeDirection, out hitInfo) ? hitInfo.collider.gameObject : null;
        }
    }
}