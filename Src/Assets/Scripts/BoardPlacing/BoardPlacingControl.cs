﻿using System;
using Assets.Scripts.Log;
using Assets.Scripts.Signal;
using UnityEngine;
using Zenject;
using Console = Assets.Scripts.Log.Console;

namespace Assets.Scripts.BoardPlacing
{

    public class BoardPlacingControl : ITickable, IBoardPlacingControl
    {
        private readonly ChessBoard _chessBoard;
        private readonly Camera _camera;
        private readonly SpatialMapping _spatialMapping;
        private readonly IResizeImageGroup _resizeImageGroup;
        private bool _placing = false;
        private Action _onCompleted = () => {};
        private bool _resizing = false;
        private Vector3 _magnitude;
        private Vector3 _originalScale;

        public BoardPlacingControl(GestureSignal gestureSignal, 
                                    ChessBoard chessBoard, 
                                    Camera camera, 
                                    SpatialMapping spatialMapping, 
                                    IResizeImageGroup resizeImageGroup)
        {
            _chessBoard = chessBoard;
            _camera = camera;
            _spatialMapping = spatialMapping;
            _resizeImageGroup = resizeImageGroup;
            _originalScale = _chessBoard.gameObject.transform.localScale;


            gestureSignal.Event += gesture =>
            {
//                if (gesture.EventType == Signal.Gesture.Type.Tapped && _placing)
//                    StopPlacing();
//
//                if (gesture.EventType == Signal.Gesture.Type.Tapped && _resizing)
//                    StopResizing();

                var manipulationGesture = gesture as ManipulationGesture;
                if (manipulationGesture!=null && _resizing)
                {
                    _magnitude = manipulationGesture.Position;
                    var f =0.25f+( Math.Abs(_magnitude.x) + Math.Abs(_magnitude.y)+ Math.Abs(_magnitude.z));
                    _chessBoard.gameObject.transform.localScale = new Vector3(_originalScale.x*f,_originalScale.y*f,_originalScale.z*f);
                    
                }
            };
        }

        private void StopPlacing()
        {
            _spatialMapping.DrawVisualMeshes = false;
            _placing = false;
            _onCompleted();
        }

        public void StartResizing(Action onCompleted)
        {
            _resizing = true;
            _resizeImageGroup.Show();
        }

        public void StopResizing()
        {
            _resizing = false;
            _resizeImageGroup.Hide();
        }

        public void StartPlacing(Action onCompleted)
        {
            _onCompleted = onCompleted;
            _placing = true;
            _spatialMapping.DrawVisualMeshes = true;

            StartResizing(() => { });
        }

        void IBoardPlacingControl.StopPlacing()
        {
            if (_placing)
            {
                StopPlacing();
                StopResizing();
            }      
        }

        public void Tick()
        {
            // If the user is in placing mode,
            // update the placement to match the user's gaze.

            if (_placing)
            {
                // Do a raycast into the world that will only hit the Spatial Mapping mesh.
                var headPosition = _camera.transform.position;
                var gazeDirection = _camera.transform.forward;

                RaycastHit hitInfo;
                if (Physics.Raycast(headPosition, gazeDirection, out hitInfo,
                    30.0f, SpatialMapping.PhysicsRaycastMask))
                {
                    // Move this object's parent object to
                    // where the raycast hit the Spatial Mapping mesh.
                    _chessBoard.gameObject.transform.position = hitInfo.point;

                    // Rotate this object's parent object to face the user.
                    var toQuat = _camera.transform.localRotation;
                    toQuat.x = 0;
                    toQuat.z = 0;
                    _chessBoard.gameObject.transform.rotation = toQuat;
                }
            }
        }
    }
}