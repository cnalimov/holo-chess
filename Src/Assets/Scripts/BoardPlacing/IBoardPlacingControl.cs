﻿using System;

namespace Assets.Scripts.BoardPlacing
{
    public interface IBoardPlacingControl
    {
        void StartPlacing(Action onCompleted);
        void StopPlacing();

        void StartResizing(Action onCompleted);
        void StopResizing();
    }
}