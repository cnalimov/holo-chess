﻿using System;
//using System.Timers;
using Assets.Scripts;
using Assets.Scripts.BoardPlacing;
using Assets.Scripts.Signal;
using UnityEngine.EventSystems;
using Zenject;

public class GameManager : IInitializable, IDisposable, ITickable
{
    private readonly GameSignal _signal;
    private readonly ChessGame _chessGame;
    private readonly IBoardPlacingControl _boardPlacing;

    public GameManager(GameSignal signal, ChessGame chessGame, IBoardPlacingControl boardPlacing)
    {
        _signal = signal;
        _chessGame = chessGame;
        _boardPlacing = boardPlacing;
    }

    public void Initialize()
    {
        _signal.Event += OnGameEvent;
        SetGameMode(GameMode.NewGame);
    }

    public void Dispose() 
    {
        _signal.Event -= OnGameEvent;
    }

    private void OnGameEvent(GameMode mode)
    {
        SetGameMode(mode);
    }

    private  void SetGameMode(GameMode mode)
    {
        switch (mode)
        {
            case GameMode.Initial:
                _chessGame.Hide();
                break;
            case GameMode.PlaceBoard:
                _chessGame.Show();
                _chessGame.Stop();
                _boardPlacing.StartPlacing(OnBoardPlacingCompleted);
                break;
            case GameMode.NewGame:
                _chessGame.Show();
                _boardPlacing.StopPlacing();
                _chessGame.StartNewGame();
                break;
//            case GameMode.Resize:
//                _chessGame.Show();
//                _boardPlacing.StartResizing(() => { });
//                break;
            default:
                throw new ArgumentOutOfRangeException("mode", mode, null);
        }
    }

    private void OnBoardPlacingCompleted()
    {
        SetGameMode(GameMode.NewGame);
    }

    public void Tick()
    {
    }
}
