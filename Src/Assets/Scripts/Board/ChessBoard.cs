﻿using System;
using UniRx;
using UnityEngine;
using Zenject;

public class ChessBoard : MonoBehaviour {

    public Transform Board;
    public Transform Checker;

    public IObservable<ChessBoard> BoardInitializedObservable
    {
        get { return _boardInitialized; }
    }

    private readonly ISubject<ChessBoard> _boardInitialized = new ReplaySubject<ChessBoard>();

    private PlayerFigures.Factory _factory;

    // Use this for initialization
    void Start ()
	{
	    Board = transform.FindChild("Chess_Board");
	    Checker = transform.FindChild("Chess_Board/Chess_Checker");
	    SetAllFigures();
        _boardInitialized.OnNext(this);
        _boardInitialized.OnCompleted();
        
	}

    // Update is called once per frame
    void Update ()
    {
	}

    [Inject]
    public void Construct(PlayerFigures.Factory factory)
    {
        _factory = factory;
    }

    private void SetAllFigures()
    {
        White = _factory.CreateInitialPosition(Board, Side.White);
        Black = _factory.CreateInitialPosition(Board, Side.Black);
    }

    public PlayerFigures Black { get; set; }
    public PlayerFigures White { get; set; }

    public Figure HighlightedFigure { get; set; }
    public Figure SelectedFigure { get; private set; }

    public Figure SelectFigure(string coordinates, Side side)
    {
        Figure figure = null;
        if (!String.IsNullOrEmpty(coordinates))
        {
            var playerFigures = side == Side.White ? White : Black;
            figure = playerFigures.Find(coordinates);
        }
        SelectFigure(figure);
        return figure;
    }
    public void SelectFigure(Figure figure)
    {
        if (figure != null)
            figure.Select();

        if (SelectedFigure != null)
            SelectedFigure.Reset();
        SelectedFigure = figure;
    }

    public bool MoveSelectedTo(string coordinates)
    {
        var coordinate = Coordinate.Create(coordinates);
        return MoveSelectedTo(coordinate);
    }
    public bool MoveSelectedTo(Coordinate coordinate)
    {
        if (coordinate!=null && SelectedFigure!=null)
        {
            CaptureOpponentFigureIfNeeded(coordinate);
            if (IsCastlingMove(coordinate))
            {
                MoveRookWhileCastle(coordinate);
            }

            SelectedFigure.MoveTo(coordinate);
            return true;
        }
        return false;
    }

    private void MoveRookWhileCastle(Coordinate kingPosition)
    {
        var kingSizeCastling = Coordinate.IndexToLetter(kingPosition.X) == 'g';
        var line = SelectedFigure.Side == Side.Black ? 8 : 1;
        var figures = SelectedFigure.Side == Side.Black ? Black : White;
        Figure rook;
        Coordinate rookPosition;
        if (kingSizeCastling)
        {
            rook = figures.Find("h" + line);
            rookPosition = Coordinate.Create("f"+line);
        }
        else
        {
            rook = figures.Find("a" + line);
            rookPosition = Coordinate.Create("d" + line);
        }
        rook.MoveTo(rookPosition);
    }

    private bool IsCastlingMove(Coordinate coordinate)
    {
        
        if (SelectedFigure!=null && SelectedFigure.Type == FigureType.King )
        {
            var moveToCol = Coordinate.IndexToLetter(coordinate.X);
            var kingPosition = SelectedFigure.Coordinate.ToStringPosition();
            if ((kingPosition == "e1" || kingPosition == "e8") && (moveToCol == 'g' || moveToCol == 'c'))
                return true;
        }
        return false;
    }

    private void CaptureOpponentFigureIfNeeded(Coordinate coordinate)
    {
        var position = coordinate.ToStringPosition();
        var figure = White.Find(position) ?? Black.Find(position);
        if (figure != null)
            figure.Capture();
    }

    public void HighlightFigure(Coordinate coordinate)
    {
        Figure figure=null;
        if (coordinate!=null && (SelectedFigure == null || SelectedFigure.Coordinate != coordinate))
        {
            var position = coordinate.ToStringPosition();
            figure = White.Find(position) ?? Black.Find(position);
        }
        HighlightFigure(figure);
    }
    public void HighlightFigure(Figure figure)
    {
        if (figure!=null)
            figure.Highlight();

        if (HighlightedFigure!=null && (SelectedFigure==null || HighlightedFigure.Coordinate!=SelectedFigure.Coordinate) )
            HighlightedFigure.Reset();

        HighlightedFigure = figure;
    }
}