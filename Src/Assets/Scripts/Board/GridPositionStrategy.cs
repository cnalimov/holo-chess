using UnityEngine;

namespace Assets.Scripts.Board
{
    public class GridPositionStrategy
    {
        private readonly float _deltaX;
        private readonly float _deltaY;

        private readonly float _centerXShift;
        private readonly float _centerYShift;

        private const float RIGHT = 0.28f;
        private const float LEFT = -0.28f;

        private const float TOP = 0.28f;
        private const float BOTTOM = -0.28f;

        public GridPositionStrategy()
        {
            _deltaX = (RIGHT - LEFT)/8;
            _deltaY = (TOP - BOTTOM)/8;

            _centerXShift = _deltaX/2;
            _centerYShift = _deltaY/2;
        }

        public Coordinate ConvertPointToCoordinate(Vector3 point)
        {
            
            var x = Mathf.FloorToInt((-1*point.x + Mathf.Abs(LEFT))/_deltaX);
            var y = Mathf.FloorToInt((point.y + Mathf.Abs(BOTTOM))/_deltaY);

            //Debug.Log($"Px: {point.x}, Py: {point.y}; x:{x}, y:{y}");
            return Coordinate.Create(x,y);
        }

        public Vector3 CoordinateToPosition(Coordinate coordinate)
        {
            var x = LEFT + (coordinate.X *_deltaX) +_centerXShift;
            var z = BOTTOM + (coordinate.Y*_deltaY) + _centerYShift;
            return new Vector3(x, 0, z);
        }
    }
}