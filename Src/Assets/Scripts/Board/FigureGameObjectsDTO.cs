﻿using System.Collections.Generic;
using UnityEngine;

public class FigureGameObjectsDTO
{
    public FigureGameObjectsDTO()
    {
    }

    public List<Transform> Pawns { get;  set; }

    public List<Transform> Bishops { get;  set; }

    public List<Transform> Knights { get;  set; }

    public List<Transform> Rocks { get;  set; }

    public Transform Queen { get;  set; }

    public Transform King { get;  set; }
}