using System;
using System.Collections.Generic;

public class Coordinate:IEquatable<Coordinate>
{
    private Coordinate(){}

    private static List<char> X_Coordinates = new List<char> { 'a','b','c','d','e','f','g','h' };
    private static List<char> Y_Coordinates = new List<char> { '1','2','3','4','5','6','7','8' };

    public int X { get; set; }
    public int Y { get; set; }

    public static Coordinate Create(string position)
    {
        if (position == null || position.Length!=2)
            throw new ArgumentException("Position should contain 2 letters","position");

        position = position.ToLowerInvariant();

        var result = new Coordinate
        {
            X = X_Coordinates.IndexOf(position[0]),
            Y = Y_Coordinates.IndexOf(position[1])
        };

        if (result.X == -1 || result.Y == -1)
            throw new ArgumentException("position is not recognized", "position");

        return result;
        
    }

    public string ToStringPosition()
    {
        return IndexToLetter(X) + Y_Coordinates[Y].ToString();
    }

    public static Coordinate Create(int x, int y)
    {
        x = x > 7 ? 7 : x;
        y = y > 7 ? 7 : y;

        x = x < 0 ? 0 : x;
        y = y < 0 ? 0 : y;
        return new Coordinate
        {
            X = x,
            Y = y
        };
    }

    public static char IndexToLetter(int index)
    {
        return X_Coordinates[index];
    }

    public bool Equals(Coordinate other)
    {
        return other!=null && other.X == X && other.Y == Y;
    }

    public static bool operator ==(Coordinate x, Coordinate y)
    {
        if ((object)x == null || (object)y == null)
        {
            return (object)x == null && (object)y == null;
        }
        return x.Equals(y);
    }

    public static bool operator !=(Coordinate x, Coordinate y)
    {
        return !(x == y);
    }
    public bool Match(string coordinates)
    {
        if (String.IsNullOrEmpty(coordinates))
            return false;
        var other = Create(coordinates);
        return Equals(other);
    }
}