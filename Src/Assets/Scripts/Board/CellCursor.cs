﻿using UnityEngine;

namespace Assets.Scripts.Board
{
    public class CellCursor : MonoBehaviour
    {

        public MeshRenderer Pointer;

        // Use this for initialization
        void Start () {
            Hide();
        }
	
        // Update is called once per frame
        void Update () {
        }

        public void Show()
        {
            Pointer.enabled = true;
        }

        public void Hide()
        {
            Pointer.enabled = false;
        }
    }
}
