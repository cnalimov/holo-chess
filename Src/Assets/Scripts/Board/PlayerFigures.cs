﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerFigures
{
    
    public List<Figure> Figures { get; set; }

    private PlayerFigures()
    {
        Figures = new List<Figure>();
    }

    private static string CalculateLine(Side side, int lineForWhite)
    {
        return (side == Side.White ? lineForWhite : 9 - lineForWhite).ToString();
    }

    public Figure Find(string coordinates)
    {
        return Figures.FirstOrDefault(figure => figure.Coordinate.Match(coordinates));
    }

    public Figure Find(Coordinate coordinate)
    {
        var stringPosition = coordinate!=null ? coordinate.ToStringPosition() : null;
        return Find(stringPosition);
    }

    public class Factory
    {
        private readonly Figure.Factory _factory;
        public Factory(Figure.Factory factory)
        {
            _factory = factory;
        }
        public  PlayerFigures CreateInitialPosition(Transform board, Side side)
        {
            var player = new PlayerFigures();
            player.Figures.Add(_factory.Create(board,  "e" + CalculateLine(side, 1),FigureType.King, side));
            player.Figures.Add(_factory.Create(board,  "d" + CalculateLine(side, 1), FigureType.Queen, side));

            player.Figures.Add(_factory.Create(board, "c" + CalculateLine(side, 1),FigureType.Bishop, side));
            player.Figures.Add(_factory.Create(board, "f" + CalculateLine(side, 1),FigureType.Bishop, side));

            player.Figures.Add(_factory.Create(board, "b" + CalculateLine(side, 1), FigureType.Knight, side));
            player.Figures.Add(_factory.Create(board, "g" + CalculateLine(side, 1), FigureType.Knight, side));

            player.Figures.Add(_factory.Create(board, "a" + CalculateLine(side, 1),FigureType.Rock, side));
            player.Figures.Add(_factory.Create(board, "h" + CalculateLine(side, 1), FigureType.Rock, side));

            var pawns = Enumerable.Range(0, 8)
                                  .Select((index) => _factory.Create(board, 
                                                                     Coordinate.IndexToLetter(index) + CalculateLine(side, 2), 
                                                                     FigureType.Pawn, 
                                                                     side));
            
            player.Figures.AddRange(pawns);
            return player;
        }
    }
}