using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Board;
using Assets.Scripts.Shared;
using UnityEngine;

public class Figure
{
    private const float SELECTION_HIGHT = 0.11f;
    private const float HIGHLIGHT_HIGHT = 0.03f;
    private readonly Transform _figure;
    private readonly TaskChainRunner _taskChainRunner;
    private readonly IAsyncProcessor _asyncProcessor;
    private readonly GridPositionStrategy _grid;
    private readonly Material _material;
    private readonly Color _originalFigureColor;
    private string _lastPositionName;
    private Vector3 _globalManipulatedPosition;

    public Side Side { get; private set; }
    public Coordinate Coordinate { get; private set; }
    public Coordinate ManipulationCoordinate { get; private set; }
    public FigureType Type { get; private set; }    
    

    private Figure(Transform figure,  Coordinate coordinate, FigureType type, Side side, TaskChainRunner taskChainRunner, IAsyncProcessor asyncProcessor, GridPositionStrategy grid)
    {
        _figure = figure;
        _taskChainRunner = taskChainRunner;
        _asyncProcessor = asyncProcessor;
        _grid = grid;
        Side = side;    
        Coordinate = coordinate;
        ManipulationCoordinate = coordinate;
        Type = type;
        _material = _figure.gameObject.GetComponent<MeshRenderer>().material;
        _originalFigureColor = _material.color;
        MoveTo(coordinate,true);
    }

    public void Select()
    {
        _taskChainRunner.Add(() =>
        {
            var endPosition = new Vector3(_figure.localPosition.x, SELECTION_HIGHT, _figure.localPosition.z);
            _globalManipulatedPosition = _figure.position;
            return MoveTo(endPosition);
        });
    }

    public void Highlight()
    {
        _material.color = new Color(0.1921f, 0.3882f, 0f, 255);
    }

    public void Reset()
    {
        _material.color = _originalFigureColor;
        _taskChainRunner.Add(() =>
        {
            var endPosition = new Vector3(_figure.localPosition.x, 0, _figure.localPosition.z);
            _globalManipulatedPosition = _figure.position;
            ManipulationCoordinate = Coordinate;
            return MoveTo(endPosition);
        });
    }

    public void MoveTo(Coordinate cell, bool withoutAnimation = false)
    {
        Func<IEnumerator> moveFunction = () =>
        {
            var deltaX = (cell.X*0.49f/7);
            var deltaZ = (cell.Y*0.49f/7);
            var endPosition = new Vector3(0.245f - deltaX, 0, 0.245f - deltaZ);
            Coordinate = cell;
            ManipulationCoordinate = Coordinate;
            return MoveTo(endPosition,(withoutAnimation)?(float?) 0:null);
        };

        if (withoutAnimation)
            _asyncProcessor.Run(moveFunction());
        else
            _taskChainRunner.Add(moveFunction);
        
    }

    public void Capture()
    {
        Coordinate = Coordinate.Create(-1,-1);
        ManipulationCoordinate = Coordinate;
        _figure.gameObject.SetActive(false);
    }

    private IEnumerator MoveTo(Vector3 endPosition,float? seconds = null)
    {
        yield return MoveOverSeconds(endPosition, seconds??0.1f);
    }

    private static IEnumerator WaitForSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
    }

    private IEnumerator MoveOverSeconds(Vector3 end, float seconds)
    {   
        float elapsedTime = 0;
        var startingPos = _figure.localPosition;
        while (elapsedTime < seconds)
        {
            _figure.localPosition = Vector3.Lerp(startingPos, end, (elapsedTime/seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        _figure.localPosition = end;
    }

    public void Manipulate(Vector3 moveVector)
    {
        _figure.position += moveVector;
    }


    public void ManipulateWithGrid(Vector3 moveVector, List<cgSimpleMove> legalMoves)
    {
        _globalManipulatedPosition += moveVector;
        var figureLocalPosition = _figure.parent.InverseTransformPoint(_globalManipulatedPosition);
        var x = figureLocalPosition.x;
        var y = figureLocalPosition.z;

        //shift zero
        x = (0.245f - x);
        y = (0.245f - y);
        //calculate cell 
        var cellSize = 0.49f / 7;
        x = Mathf.RoundToInt(x / cellSize).Bound(0, 7);
        y = Mathf.RoundToInt(y / cellSize).Bound(0, 7);

        var coordinate = Coordinate.Create((int)x,(int)y);
        if (coordinate != Coordinate && !coordinate.MoveIsLegal(legalMoves))
        {
            _globalManipulatedPosition -= moveVector;
            return;
        }
        ManipulationCoordinate = coordinate;
        var deltaX = (x * 0.49f / 7);
        var deltaZ = (y * 0.49f / 7);
        var endPosition = new Vector3(0.245f - deltaX, figureLocalPosition.y, 0.245f - deltaZ);
        _figure.localPosition = endPosition;
    }

    public class Factory 
    {
        private readonly IAsyncProcessor _processor;
        private readonly TaskChainRunner _chainRunner;
        private readonly ChessInstaller.FigurePrefabs _figurePrefabs;
        private readonly GridPositionStrategy _gridPositionStrategy;

        public Factory(IAsyncProcessor processor, TaskChainRunner chainRunner, ChessInstaller.FigurePrefabs figurePrefabs, GridPositionStrategy gridPositionStrategy)
        {
            _processor = processor;
            _chainRunner = chainRunner;
            _figurePrefabs = figurePrefabs;
            _gridPositionStrategy = gridPositionStrategy;
        }

        public Figure Create(Transform board, string coordinate, FigureType type, Side side)
        {
            var figureCoordinate = Coordinate.Create(coordinate);
            var prefab = ResolvePrefab(type);
//            var prefab = AssetDatabase.LoadAssetAtPath(prefabName, typeof(GameObject));
//            var prefab = Resources.Load(prefabName, typeof(GameObject));
            var gameObject = (GameObject)GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
            
            gameObject.transform.RotateAround(gameObject.transform.position, Vector3.left,90);
            gameObject.transform.SetParent(board,false);
            var renderer = gameObject.GetComponent<MeshRenderer>();
            var material = new Material( renderer.material);
            renderer.material = material;
            renderer.material.color = side ==Side.Black ? Color.black : new Color(0.5f,0.5f,0.5f,1);
            if (side == Side.Black)
                gameObject.transform.RotateAround(gameObject.transform.position,Vector3.up,180);

            return new Figure(gameObject.transform, figureCoordinate, type, side, _chainRunner,_processor,_gridPositionStrategy);
        }

        private GameObject ResolvePrefab(FigureType figureType)
        {
            switch (figureType)
            {
                case FigureType.Pawn:
                    return _figurePrefabs.Pawn;
                case FigureType.Knight:
                    return _figurePrefabs.Knight;
                case FigureType.Bishop:
                    return _figurePrefabs.Bishop;
                case FigureType.Rock:
                    return _figurePrefabs.Rock;
                case FigureType.Queen:
                    return _figurePrefabs.Queen;
                case FigureType.King:
                    return _figurePrefabs.King;
                default:
                    throw new ArgumentOutOfRangeException("figureType", figureType, null);
            }
        }
    }
}

public enum FigureType
{
    Pawn,
    Knight,
    Bishop,
    Rock,
    Queen,
    King    
}

public enum Side
{
    White,
    Black
}


