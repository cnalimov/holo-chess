using System;
using Assets.Scripts.Signal;
using UniRx;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Board
{
    public interface ICellSelector
    {
        IObservable<Coordinate> GazeCellObservable { get; }
        IObservable<Coordinate> VoiceCellObservable { get;  }
        Coordinate GazeCell { get; }
        void Hide();
        void Show();
        void SetColor( CellSelector.Color color);
    }

    public class CellSelector: ITickable, IInitializable, ICellSelector
    {
        private readonly CellCursor _cursor;
        private readonly Camera _camera;
        private readonly ChessBoard _chessBoard;
        private readonly GridPositionStrategy _grid;
        private Transform _checker;
        private string _checkerName;
        private Coordinate _gazeCell;
        private readonly Subject<Coordinate> _gazeCellSubject = new Subject<Coordinate>();
        private readonly Subject<Coordinate> _voiceCellObservable  = new Subject<Coordinate>();
        private bool _isVisible;
        private MeshRenderer _meshRenderer;
        private int _layerMask;
        private bool _handDetected;

        public CellSelector(CellCursor cursor, Camera camera, ChessBoard chessBoard, GridPositionStrategy grid, GestureSignal gestureSignal, CellSelectionSignal cellSelectionSignal )
        {
            _cursor = cursor;
            _camera = camera;
            _chessBoard = chessBoard;
            _grid = grid;
            _handDetected = true;
            gestureSignal.Event += guesture =>
            {
                if (guesture.EventType == Signal.Gesture.Type.HandLost)
                    ToggleHandDetected(false);
                if (guesture.EventType == Signal.Gesture.Type.HandDetected)
                    ToggleHandDetected(true);
            };
            cellSelectionSignal.Event += coordinate =>
            {
                _voiceCellObservable.OnNext(coordinate);
            };
            Show();
        }

        private void ToggleHandDetected(bool handDetected)
        {
            //Don't toggle cursor on hand
           /* _handDetected = handDetected;
            if (!_handDetected)
              HideCursor();*/
        }


        public IObservable<Coordinate> GazeCellObservable
        {
            get { return _gazeCellSubject; }
        }

        public IObservable<Coordinate> VoiceCellObservable
        {
            get { return _voiceCellObservable; }
        }

        public Coordinate GazeCell
        {
            get
            {
                return _gazeCell;
            }
            private set
            {
                if (_gazeCell != value)
                {
                    _gazeCell = value;
                    MoveCursorGazeCell();
                    _gazeCellSubject.OnNext(_gazeCell);
                }
            }
        }

        public void Show()
        {
            _isVisible = true;

        }

        public void SetColor(Color color)
        {
            switch (color)
            {
                case Color.Disabled:
                    _meshRenderer.material.color  = new UnityEngine.Color(0.447f, 0.447f, 0.447f, 1);
                    break;
                case Color.Active:
                    _meshRenderer.material.color = new UnityEngine.Color (0.1921f, 0.3882f, 0f, 255);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("color", color, null);
            }
        }

        public void Hide()
        {
            _isVisible = false;
            HideCursor();
        }

        private void MoveCursorGazeCell()
        {
            if (_gazeCell!=null)
            {
                var position = _grid.CoordinateToPosition(_gazeCell);
                _cursor.gameObject.transform.localPosition = position;
            }
        }


    
        public void Tick()
        {
            if (!_isVisible || !_handDetected)
                return;

            // Do a raycast into the world based on the user's
            // head position and orientation.
            var headPosition = _camera.transform.position;
            var gazeDirection = _camera.transform.forward;

            RaycastHit hitInfo;
            
            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 
                35.0f, _layerMask))
            {
                if (CheckerIsHit(hitInfo))
                {
                    var point = hitInfo.point;
                    var relativePoint = _chessBoard.Checker.InverseTransformPoint(point);
                    GazeCell = _grid.ConvertPointToCoordinate(relativePoint);
                    _cursor.Show();
                    return;
                }
            }
            HideCursor();
        }

        private void HideCursor()
        {
            GazeCell = null;
//            _cursor.Hide();
        }

        private bool CheckerIsHit(RaycastHit hitInfo)
        {
            var checkerName = GetCheckerName();
            var gameObjectName = hitInfo.transform.gameObject.name;
            return gameObjectName == GetCheckerName();
        }

        private string GetCheckerName()
        {
            return _checkerName ?? (_checkerName = _chessBoard.Checker != null ? _chessBoard.Checker.name : null);
        }

        public void Initialize()
        {
            _chessBoard.BoardInitializedObservable.Subscribe(board =>
            {
                _checker = board.Checker;
//                _checker = board.Board;
                _layerMask = 1 << _checker.gameObject.layer;
                _meshRenderer = _cursor.GetComponentInChildren<MeshRenderer>();
            });

        }

        public enum Color
        {
            Disabled,
            Active
        }

    }
}