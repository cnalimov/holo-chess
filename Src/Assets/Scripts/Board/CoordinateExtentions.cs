using System.Collections.Generic;
using System.Linq;

public static class CoordinateExtentions
{
    public static bool MoveIsLegal(this Coordinate coordinate, List<cgSimpleMove> legalMoves)
    {
        if (coordinate == null)
            return false;
        var toCellName = cgGlobal.IndexFromCellName(coordinate.ToStringPosition());
        return legalMoves.Any(m => m.to == toCellName);
    }
}